
/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/
import QtQuick 2.6
import QtQuick.Controls 2.4
import QtQuick.Dialogs 1.0
import QtQuick.Layouts 1.3

import Qt.labs.folderlistmodel 1.0

import StopMotion 1.0

ApplicationWindow {
    id: root
    visible: true
    width: 1024
    height: 760
    property int highestZ: 0
    property real listHeight: 100
    property real imageRatio: 16 / 9
    property var imageNameFilters: ["*.png", "*.jpg", "*.svg"]
    property string picturesLocation: ""
    property string outputFormat: "image_%d.jpg"
    property string outputResolution: "1920x1080"
    property int framePerSecond: 15
    property real numberOfImage: 2
    property real overlayOpacity: 0.25
    property int lastIndex: -1


    Column {
        z:100
        opacity: raspistill.isRunning ? 0.75 : 1
        anchors.right: parent.right
        anchors.top: parent.top
        Button {
            text: "Project"
            onClicked: {
                projectMenu.y = y + height
                projectMenu.open()
            }
        }
        Menu {
            id: projectMenu
            enabled: !raspistill.isRunning
            Button { text: qsTr("frame per second"); enabled: false }
            SpinBox {
                from: 5
                to: 30
                value: 15
                onValueChanged: framePerSecond = value
            }
            Button { text: qsTr("image name"); enabled: false }
            TextField {
                text: "image_%d.jpg"
                onTextChanged: outputFormat = text
            }
            Button { text: qsTr("resolution"); enabled: false }
            ComboBox {
                model: ListModel {
                    ListElement {
                        key: "1920x1080"
                    }
                }
                onAccepted: outputResolution = currentValue
            }
            Button {
                text: qsTr("Open")
                onClicked: fileDialog.open()
            }
        }
        Button {
            text: raspistill.isRunning ? qsTr("Stop") : qsTr("Start")
            onClicked: raspistill.isRunning ? raspistill.stop()  : raspistill.start()
            enabled: folderModel.folder != ""
        }
        Button {
            text: "Parameters"
            onClicked: {
                parameterMenu.y = y + height
                parameterMenu.open()
            }
        }
        Menu {
            id: parameterMenu
            Button { text: qsTr("number of overlays"); enabled: false;}
            SpinBox {
                from: 1
                to: 4
                value: 2
                onValueChanged: numberOfImage = value
            }
            Button { text: qsTr("overlay opacity"); enabled: false }
            SpinBox {
                from: 10
                to: 100
                value: 25
                stepSize: 10
                onValueChanged: overlayOpacity = value / 100
            }
        }
        Button {
            text: "Capture"
            enabled: raspistill.isRunning
            onClicked: raspistill.triggerShot()
        }
        Button {
            text: "Delete"
            enabled: raspistill.isRunning
            onClicked: raspistill.deleteCurrent()
        }
    }

    Raspistill {
        id: raspistill

        function deleteCurrent() {
            raspistill.deleteShot(folderModel.get(listView.currentIndex,"fileName"))
        }
    }

    FileDialog {
        id: fileDialog
        title: "Choose a folder with some images"
        selectFolder: true
        folder: picturesLocation
        onAccepted: {
            // this is the start up sequence
            folderModel.folder = fileUrl + "/"
            raspistill.imageFolder = fileUrl + "/"
            projectMenu.close()
            // move window to the right
            root.setX(root.screen.width - root.width)
            root.setY((root.screen.height - root.height)/2)
            // setup raspistill preview to the left
            var pSize = Qt.vector2d(root.x, root.x / imageRatio)
            var pPoint = Qt.vector2d(0, (root.screen.height - pSize.y)/2)
            raspistill.previewRectangle = pPoint.x + ',' + pPoint.y + ',' + pSize.x + ',' + pSize.y
            raspistill.start()

        }
    }

    FolderListModel {
        id: folderModel
        objectName: "folderModel"
        showDirs: false
        nameFilters: imageNameFilters
        sortField: FolderListModel.Name
        onFolderChanged: {
            listView.currentIndex = count - 1
        }
        onRowsAboutToBeRemoved: {
            lastIndex = listView.currentIndex
        }
        onRowsAboutToBeInserted: {
            if(lastIndex == count - 2) {
                lastIndex += 1
            }
        }
    }

    Repeater {
        model: folderModel
        Rectangle {
            id: photoFrame
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.bottom: listView.top
            z: -index
            opacity: index == listView.currentIndex ? 1.0 : (overlayOpacity / numberOfImage)
            color: "transparent"
            Component {
                id: imageLoader
                Image {
                    id: image
                    anchors.fill: parent
                    fillMode: Image.PreserveAspectFit
                    source: folderModel.folder + fileName
                    antialiasing: true
                }
            }
            Component {
                id: emptyLoader
                Rectangle {
                    color: "transparent"
                }
            }
            Loader {
                anchors.fill: parent
                sourceComponent: index > listView.currentIndex - numberOfImage
                                 && index <= listView.currentIndex && fileName ? imageLoader : emptyLoader
            }
        }
    }

    ListView {
        id: listView
        width: parent.width
        height: listHeight
        anchors.bottom: parent.bottom
        orientation: ListView.Horizontal
        focus: true
        model: folderModel
        highlightFollowsCurrentItem: false
        interactive: true

        ScrollBar.horizontal: ScrollBar {}
        //enabled: false
        delegate: Rectangle {
            id: listItem
            width: listView.height * imageRatio
            height: listView.height
            Image {
                id: image
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: folderModel.folder + fileName
                Label {
                    z:100
                    anchors.centerIn:  parent
                    text: index
                    background: Rectangle{
                        anchors.centerIn: parent
                        width: 30
                        height: 30
                        color: "orange"
                        opacity: index == listView.currentIndex ? 1 : 0.5
                        radius: 10
                    }
                }
            }
            MouseArea {
                anchors.fill: parent
                onClicked: listView.currentIndex = index
            }
        }

        function next(increment) {
            currentIndex = Math.min(currentIndex + increment, count - 1)
        }

        function previous(increment) {
            currentIndex = Math.max(currentIndex - increment, 0)
        }

        onCurrentIndexChanged: {
            if(lastIndex != -1) {
                currentIndex = Math.max(0,Math.min(lastIndex, count - 1))
                lastIndex = -1
            }
            positionViewAtIndex(currentIndex, ListView.Center)
        }
    }

    Label {
        visible: listView.currentIndex != -1
        anchors.margins: 10
        anchors.left: parent.left
        anchors.top: parent.top
        text: "frame " + listView.currentIndex + " (" + (listView.currentIndex / framePerSecond).toFixed(1) + " sec)"
        background: Rectangle{
            anchors.margins: -5
            anchors.fill: parent
            color: "orange"
            radius: 5
        }
    }

    Shortcut {
        sequence: StandardKey.MoveToNextChar
        onActivated: listView.next(1)
    }

    Shortcut {
        sequence: StandardKey.MoveToPreviousChar
        onActivated: listView.previous(1)
    }

    Shortcut {
        sequence: StandardKey.MoveToNextPage
        onActivated: listView.next(10)
    }

    Shortcut {
        sequence: StandardKey.MoveToPreviousPage
        onActivated: listView.previous(10)
    }

    Shortcut {
        sequence: StandardKey.Delete
        onActivated: raspistill.deleteCurrent()
    }

    Shortcut {
        sequence: StandardKey.InsertParagraphSeparator
        onActivated: raspistill.triggerShot()
    }

    Shortcut {
        sequence: StandardKey.Quit
        onActivated: Qt.quit()
    }
}
