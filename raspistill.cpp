﻿#include "raspistill.h"

#include <QProcess>
#include <QDebug>

#include <csignal>

Raspistill::Raspistill() :
    m_running(false),
    m_imageNameFormat("image_%d.jpg"),
    m_imageFolder(""),
    m_imageSize("1920x1080"),
    m_previewRectangle("0,0,480,270")
{
    m_rapspistillProcess = new QProcess(this);
    connect(m_rapspistillProcess, &QProcess::stateChanged, this, &Raspistill::onProcessStateChanged);
    connect(m_rapspistillProcess, &QProcess::readyRead, this, &Raspistill::readProcess);
}

void Raspistill::setImageFolder(const QString &folder)
{
    m_imageFolder = folder;
    m_imageFolder.remove("file://");
}

void Raspistill::setRunning(bool running)
{
    m_running = running;
    emit runningChanged();
}

void Raspistill::start()
{
    qDebug() << "Raspistill::start";
    if(m_running) return;
    QStringList arguments;
    arguments << "-p" << m_previewRectangle;
    arguments << "-w" << m_imageSize.split("x").first();
    arguments << "-h" << m_imageSize.split("x").last();
    arguments << "-o" << m_imageNameFormat;
    arguments << "-ts" << "-s";
    qDebug() << "Raspistill::start" << m_imageFolder;
    qDebug() << "Raspistill::start" << arguments;
    m_rapspistillProcess->setWorkingDirectory(m_imageFolder);
    m_rapspistillProcess->start("raspistill", arguments, QProcess::ReadWrite);
}

void Raspistill::stop()
{
    qDebug() << "Raspistill::stop";
    m_rapspistillProcess->terminate();
}

void Raspistill::triggerShot()
{
    qDebug() << "Raspistill::triggerShot";
    if(!m_running) return;
    kill(m_rapspistillProcess->pid(), SIGUSR1);
}

void Raspistill::deleteShot(const QString &filename)
{
    qDebug() << "Raspistill::deleteShot" << m_imageFolder + "/" + filename;
    QFile::remove(m_imageFolder + "/" + filename);
}

void Raspistill::onProcessStateChanged(QProcess::ProcessState state)
{
    qDebug() << "Raspistill::onProcessStateChanged" << state;
    switch(state) {
    case QProcess::NotRunning:
        emit stopped();
        setRunning(false);
        return;
    case QProcess::Running:
        emit started();
        setRunning(true);
        return;
    default:
        return;
    }
}

void Raspistill::readProcess()
{
    qDebug() << "readProcess" << m_rapspistillProcess->readAll();
}

