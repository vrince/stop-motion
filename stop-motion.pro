TEMPLATE = app

QT += qml quick
qtHaveModule(widgets): QT += widgets

SOURCES += main.cpp \
    raspistill.cpp
RESOURCES += stop-motion.qrc

ICON = resources/icon.png

HEADERS += \
    raspistill.h

