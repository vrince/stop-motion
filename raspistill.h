#ifndef RASPISTILL_H
#define RASPISTILL_H

#include <QQuickItem>
#include <QSize>
#include <QRect>
#include <QProcess>

class Raspistill : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(bool isRunning READ running NOTIFY runningChanged)
    Q_PROPERTY(QString imageNameFormat READ imageNameFormat WRITE setImageNameFormat)
    Q_PROPERTY(QString imageFolder READ imageFolder WRITE setImageFolder)
    Q_PROPERTY(QString previewRectangle READ previewRectangle WRITE setPreviewRectangle)


public:
    Raspistill();

    bool running() const { return m_running;}
    QString imageNameFormat() const { return m_imageNameFormat;}
    QString imageFolder() const { return m_imageFolder;}
    QString previewRectangle() const { return m_previewRectangle;}

    void setImageNameFormat(const QString& name) { m_imageNameFormat = name;}
    void setImageFolder(const QString& folder);
    void setPreviewRectangle(const QString& rectangle) { m_previewRectangle = rectangle;}

private:
    void setRunning(bool running);

public slots:
    void start();
    void stop();
    void triggerShot();
    void deleteShot(const QString& filename);

private slots:
    void onProcessStateChanged(QProcess::ProcessState state);
    void readProcess();

signals:
    void started();
    void stopped();
    void runningChanged();

private:
    bool m_running;
    QString m_imageNameFormat;
    QString m_imageFolder;
    QString m_imageSize;
    QString m_previewRectangle;

    QProcess* m_rapspistillProcess;
};

#endif // RASPISTILL_H
